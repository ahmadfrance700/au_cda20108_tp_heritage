package fr.afpa.beans;

public class MantARisque extends Manutentionnaire implements IEmployeARisque {

	public MantARisque(String nom, String prenom, int age, String dateEntree, int nombreHeurs) {
		super(nom, prenom, age, dateEntree, nombreHeurs);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getPrime() {
		return IEmployeARisque.prime;
	}

	@Override
	public double calculeeSalaire() {
		return super.calculeeSalaire() + IEmployeARisque.prime;
	}

}
