package fr.afpa.beans;

public class Technicien extends Employe {

	private int nombreProduits;

	public Technicien(String nom, String prenom, int age, String dateEntree, int nombreProduits) {
		super(nom, prenom, age, dateEntree);
		this.nombreProduits = nombreProduits;
		setSalaire(calculeeSalaire());
	}

	@Override
	public String getNom() {
		return "Le producteur " + super.getNom() + " " + super.getPrenom();
	}

	@Override
	public double calculeeSalaire() {
		return nombreProduits * 5;
	}

}
