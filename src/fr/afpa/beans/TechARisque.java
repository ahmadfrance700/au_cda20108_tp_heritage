package fr.afpa.beans;

public class TechARisque extends Technicien implements IEmployeARisque {

	public TechARisque(String nom, String prenom, int age, String dateEntree, int nombreProduits) {
		super(nom, prenom, age, dateEntree, nombreProduits);
	}

	@Override
	public double getPrime() {
		return IEmployeARisque.prime;
	}

	@Override
	public double calculeeSalaire() {
		// TODO Auto-generated method stub
		return super.calculeeSalaire() + IEmployeARisque.prime;
	}

}
