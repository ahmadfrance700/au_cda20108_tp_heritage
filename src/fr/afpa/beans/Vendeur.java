package fr.afpa.beans;

public class Vendeur extends Employe {

	private double chiffreAffaire;

	public Vendeur(String nom, String prenom, int age, String dateEntree, double chiffreAffaire) {
		super(nom, prenom, age, dateEntree);
		this.chiffreAffaire = chiffreAffaire;
		setSalaire(calculeeSalaire());
	}

	@Override
	public double calculeeSalaire() {
		return 400 + chiffreAffaire * 0.2;
	}

	@Override
	public String getNom() {
		return "Le vendeur " + super.getNom() + " " + super.getPrenom();
	}

	public double getChiffreAffaire() {
		return chiffreAffaire;
	}

	public void setChiffreAffaire(double chiffreAffaire) {
		this.chiffreAffaire = chiffreAffaire;
	}

}
