package fr.afpa.beans;

public class Representant extends Vendeur {

	public Representant(String nom, String prenom, int age, String dateEntree, double chiffreAffaire) {
		super(nom, prenom, age, dateEntree, chiffreAffaire);
		setSalaire(calculeeSalaire());
	}

	@Override
	public double calculeeSalaire() {
		return getChiffreAffaire() * 0.2 + 800;
	}

	@Override
	public String getNom() {
		return "Le representant " + super.getNom() + " " + super.getPrenom();
	}

}
