package fr.afpa.beans;

public class Manutentionnaire extends Employe {

	private int nombreHeurs;

	public Manutentionnaire(String nom, String prenom, int age, String dateEntree, int nombreHeurs) {
		super(nom, prenom, age, dateEntree);
		this.nombreHeurs = nombreHeurs;
		setSalaire(calculeeSalaire());

	}

	@Override
	public double calculeeSalaire() {
		return nombreHeurs*20;
	}

	@Override
	public String getNom() {
		return "Le manutentionaire " + super.getNom() + " " + super.getPrenom();
	}

}
