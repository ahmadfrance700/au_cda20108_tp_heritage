package fr.afpa.app;

import java.text.DecimalFormat;

import fr.afpa.beans.MantARisque;
import fr.afpa.beans.Manutentionnaire;
import fr.afpa.beans.Representant;
import fr.afpa.beans.TechARisque;
import fr.afpa.beans.Technicien;
import fr.afpa.beans.Vendeur;
import fr.afpa.services.Personnel;

public class Application {

	public static void main(String[] args) {
		Personnel p = new Personnel();
		p.ajouterEmploye(new Vendeur("Pierre", "Business", 45, "1995", 30000));
		
		p.ajouterEmploye(new Representant("L�on", "Vendtout", 25, "2001", 20000));
		p.ajouterEmploye(new Technicien("Yves", "Bosseur", 28, "1998", 1000));
		p.ajouterEmploye(new Manutentionnaire("Jeanne", "Stocketout", 32, "1998", 45));
		p.ajouterEmploye(new TechARisque("Jean", "Flippe", 28, "2000", 1000));
		p.ajouterEmploye(new MantARisque("Al", "Abordage", 30, "2001", 45));
		p.afficherSalaires();
		System.out.println("Le salaire moyen dans l'entreprise est de " + (new DecimalFormat(".00")).format(p.salaireMoyen()) + " euros.");
	}
}