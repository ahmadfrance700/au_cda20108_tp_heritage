package fr.afpa.services;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.beans.Employe;

public class Personnel {
	private List<Employe> employesTab;

	public Personnel() {
		this.employesTab = new ArrayList<Employe>();
	}

	public void ajouterEmploye(Employe employe) {
		if (employe != null) {
			employesTab.add(employe);
		}
	}

	public void afficherSalaires() {
		for (int i = 0; i < employesTab.size(); i++) {
			System.out.println(
					employesTab.get(i).getNom() + " mon salaire est " + employesTab.get(i).getSalaire() + " euros.");
		}
	}

	public double salaireMoyen() {
		double moyen = 0;
		for (int i = 0; i < employesTab.size(); i++) {
			moyen += employesTab.get(i).getSalaire();
		}
		return moyen / employesTab.size();
	}

}
